﻿using HtmlEditorController.Connectors;
using Newtonsoft.Json;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExportAllowedHtmlDocuments
{
    class Program
    {
        private static Globals globals;
        private static string exportPath;
        static void Main(string[] args)
        {
            var path = args[0];
            if (!System.IO.Directory.Exists(path))
            {
                Console.WriteLine("Syntax: ExportAllowedHtmlDocuments <ExportPath>");
                Environment.Exit(-1);
            }
            exportPath = args[0];
            globals = new Globals();
            var result = ExportHtmlDoc();


            if (result.GUID == globals.LState_Error.GUID)
            {
                Console.WriteLine("Error while exporting");
                Environment.Exit(-1);
            }

        }

        private static clsOntologyItem ExportHtmlDoc()
        {
            var connector = GetHtmlEditorConnector();
            var result = globals.LState_Success.Clone();

            try
            {

                Console.Write("\tCreate Anonymous");
                var filePath = System.IO.Path.Combine(exportPath, "anonymoushtmldocuments.json");
                System.IO.File.WriteAllText(filePath, Newtonsoft.Json.JsonConvert.SerializeObject(connector.AllowedItems, Newtonsoft.Json.Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), Encoding.UTF8);
                Console.Write("\tCreated Anonymous");

                foreach (var allowedItem in connector.AllowedItems)
                {
                    Console.Write("Export Document: " + allowedItem.GUID);
                    clsObjectAtt oAItemHtmlDocument = null;
                    var connectorResult = connector.GetHtmlDocument(allowedItem.GUID);
                    connectorResult.Wait();
                    if (connectorResult.Result.Result.GUID == connector.LocalConfig.Globals.LState_Success.GUID)
                    {
                        oAItemHtmlDocument = connectorResult.Result.OAItemHtmlDocument;
                    }

                    Console.Write("\tCreate Html");
                    var regexChange = new Regex("href=\"HtmlEditorJQ.html.Class=" + connector.LocalConfig.Globals.RegexGuid + "(.|.{5})Object=" + connector.LocalConfig.Globals.RegexGuid + "\"");
                    var regexObject = new Regex("(O|o)bject=" + connector.LocalConfig.Globals.RegexGuid);
                    var regexObjectId = new Regex(connector.LocalConfig.Globals.RegexGuid);
                    var contentBuilder = new StringBuilder();
                    if (oAItemHtmlDocument != null && oAItemHtmlDocument.Val_String != null)
                    {
                        contentBuilder.AppendLine("<div id='title" + oAItemHtmlDocument.ID_Object + "' style='visibility:hidden'>" + oAItemHtmlDocument.Name_Object + "</div>");
                        var objectId = "";
                        var text =  oAItemHtmlDocument.Val_String;
                        var matches = regexChange.Matches(text);
                        if (matches.Count > 0)
                        {
                            var start = 0;
                            matches.Cast<Match>().ToList().ForEach(matchItem =>
                            {
                                var index = matchItem.Index;
                                var length = matchItem.Length;
                                matchItem = regexObject.Match(matchItem.Value);
                                if (matchItem.Success)
                                {
                                    var objectString = matchItem.Value;
                                    matchItem = regexObjectId.Match(objectString);
                                    if (matchItem.Success)
                                    {
                                        objectId = matchItem.Value;
                                    }

                                    contentBuilder.Append(oAItemHtmlDocument.Val_String.Substring(start, index - start));
                                    contentBuilder.Append("href=\"/iDoku/iDoku/Index/" + objectId + "\"");
                                    start = index + length;

                                }

                            });
                            if (start < text.Length)
                            {
                                contentBuilder.Append(oAItemHtmlDocument.Val_String.Substring(start));
                            }
                        }
                        else
                        {
                            contentBuilder.Append(oAItemHtmlDocument.Val_String);
                        }
                        Console.Write("\tCreated Html");
                        contentBuilder.Replace("../Resources/UserGroupRessources", "/iDoku/Resources/UserGroupRessources");
                        filePath = System.IO.Path.Combine(exportPath, oAItemHtmlDocument.ID_Object + ".html");
                        System.IO.File.WriteAllText(filePath, contentBuilder.ToString());
                        Console.Write("\tExportet Html");
                        result = WriteMetaJson(allowedItem.GUID, connector);
                        result = WriteMetaTree(allowedItem.GUID, connector);
                        Console.WriteLine();
                    }
                }

                return result;
            }
            catch (Exception)
            {

                return globals.LState_Error.Clone();
            }
            

            
        }

        private static clsOntologyItem WriteMetaTree(string idItem, HtmlEditorConnector connector)
        {
            var hierarchyResult = connector.GetHtmlHierarchy(idItem);
            hierarchyResult.Wait();

            if (hierarchyResult.Result.Result.GUID == connector.LocalConfig.Globals.LState_Error.GUID)
            {
                return globals.LState_Error.Clone();
            }

            var htmlDoc = hierarchyResult.Result.Hierarchy.FirstOrDefault(hierarchyItem => hierarchyItem.ID_Object == hierarchyResult.Result.HtmlDocumentOfRef.GUID);

            if (htmlDoc == null)
            {
                return globals.LState_Success.Clone();
            }
            var kendoTreeNode = new KendoTreeNode
            {
                NodeId = htmlDoc.ID_Object,
                NodeName = htmlDoc.Name_Object,

            };

            kendoTreeNode.SubNodes = GetTreeNodes(hierarchyResult.Result.Hierarchy, kendoTreeNode);

            Console.Write("\tCreate TreeData");
            var filePath = System.IO.Path.Combine(exportPath, idItem + "_tree.json");
            System.IO.File.WriteAllText(filePath, Newtonsoft.Json.JsonConvert.SerializeObject(new { data = new List<KendoTreeNode> { kendoTreeNode } }, Newtonsoft.Json.Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), Encoding.UTF8);
            Console.Write("\tCreated TreeData");

            return globals.LState_Success.Clone();
        }

        private static List<KendoTreeNode> GetTreeNodes(List<clsObjectRel> hierarchy, KendoTreeNode parentNode)
        {
            var subNodes = hierarchy.Where(subNode => subNode.ID_Object == parentNode.NodeId).OrderBy(subNode => subNode.OrderID).Select(subNode => new KendoTreeNode
            {
                NodeId = subNode.ID_Other,
                NodeName = subNode.Name_Other
            }).ToList();

            subNodes.ForEach(subNode =>
            {
                subNode.SubNodes = GetTreeNodes(hierarchy, subNode);
            });

            return subNodes.Any() ? subNodes : null;
        }

        private static clsOntologyItem WriteMetaJson(string idItem, HtmlEditorConnector connector)
        {
            var result = globals.LState_Success.Clone();
            var tagsTask = connector.GetTags(idItem);
            tagsTask.Wait();

            var tags = tagsTask.Result.TypedTags.Where(typedTag => typedTag.IdTagParent != "902b2cd20fe64f4992fd97bcf28e2dad"
                && typedTag.IdTagParent != "f62cf1ccee51475f9c007094f1809b56"
                && typedTag.IdTagParent != "d84fa125dbce44b091539abeb66ad27f"
                && typedTag.IdTagParent != "d0f582137db44882bd15962163015d13").OrderBy(typedTag => typedTag.OrderId).ThenBy(typedTag => typedTag.NameTag);
            var otherRefTypes = tags.Where(typedTag => typedTag.IdTagParent != "ce29760f023043daa19d91c6d82a8437" && typedTag.IdTagParent != "3bb4b05d8fcb48b7a1ac0fb42a265c38").GroupBy(typedTag => new { Id = typedTag.IdTagParent, Name = typedTag.NameTagParent }).OrderBy(refGroup => refGroup.Key.Name).ToList();

            var htmlDocs = connector.GetHtmlDocumentsByRefs(tags.Select(tag => new clsOntologyItem { GUID = tag.IdTag }).ToList());
            htmlDocs.Wait();
            var htmlDocsAllowed = (from htmlDocRel in htmlDocs.Result.HtmlDocumentsToRef
                                   join allowedDoc in connector.AllowedItems on htmlDocRel.ID_Object equals allowedDoc.GUID
                                   select htmlDocRel);

            var tagsWithReferences = (from tag in tags
                                      join htmlDoc in htmlDocsAllowed on tag.IdTag equals htmlDoc.ID_Other into htmlDocs1
                                      from htmlDoc in htmlDocs1.DefaultIfEmpty()
                                      select new TypedTagWithHtmlPage(tag, htmlDoc != null ? "/iDoku/iDoku/Index/" + htmlDoc.ID_Object : ""));

            var references = new List<Reference>();
            var sceneTags = tagsWithReferences.Where(typedTag => typedTag.IdTagParent == "ce29760f023043daa19d91c6d82a8437");

            if (sceneTags.Any())
            {
                references.Add(new Reference
                {
                    Id = "ce29760f023043daa19d91c6d82a8437",
                    Name = "Szenen",
                    Tags = sceneTags.ToList()
                });
            }

            var termTags = tagsWithReferences.Where(typedTag => typedTag.IdTagParent == "3bb4b05d8fcb48b7a1ac0fb42a265c38");

            if (termTags.Any())
            {
                references.Add(new Reference
                {
                    Id = "3bb4b05d8fcb48b7a1ac0fb42a265c38",
                    Name = "Schlagwörter",
                    Tags = termTags.OrderBy(term => term.NameTag).ToList()
                });
            }

            otherRefTypes.ForEach(refGroup =>
            {
                var reference = new Reference
                {
                    Id = refGroup.Key.Id,
                    Name = refGroup.Key.Name
                };

                reference.Tags = tagsWithReferences.Where(typedTag => typedTag.IdTagParent == refGroup.Key.Id).ToList();
                references.Add(reference);
            });


            Console.Write("\tCreate Meta");
            var filePath = System.IO.Path.Combine(exportPath, idItem + ".json");
            System.IO.File.WriteAllText(filePath, Newtonsoft.Json.JsonConvert.SerializeObject(references));
            Console.Write("\tCreated Meta");

            return result;
        }

        private static HtmlEditorConnector GetHtmlEditorConnector()
        {


            
            var htmlEditorConnector = new HtmlEditorConnector(globals);
            return htmlEditorConnector;
        }
    }
}
